import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    cart: {},
    currency: "USD",
    cartOpen: false,
    products: [],
  },
  mutations: {
    addToCart(state, product) {
      const { cart } = state;
      if (cart[product.id]) cart[product.id].numberInCart++;
      else cart[product.id] = { numberInCart: 1 };

      state.cart = { ...cart };
    },
    changeCurrency(state, currency) {
      state.currency = currency;
    },
    openCart(state) {
      state.cartOpen = true;
    },
    closeCart(state) {
      state.cartOpen = false;
    },
    updateProducts(state, products) {
      state.products = products;
    },
    decrementProduct(state, productId) {
      const { cart } = state;
      if (cart[productId]) {
        if (cart[productId].numberInCart > 1) cart[productId].numberInCart--;
        else delete cart[productId];
      }
      state.cart = { ...cart };
    },
    removeProduct(state, productId) {
      const { cart } = state;
      if (cart[productId]) delete cart[productId];
      state.cart = { ...cart };
    },
  },
  getters: {
    productsInCart: (state) => {
      const productIds = Object.keys(state.cart);
      return productIds.map((id) => {
        const product = state.products.find((product) => product.id == id);
        return {
          ...product,
          ...state.cart[id],
        };
      });
    },
    cartTotal: (state, getters) => {
      return getters.productsInCart.reduce(
        (total, product) => total + product.price * product.numberInCart,
        0
      );
    },
  },
});

export default store;
